#define _GNU_SOURCE

#include "mem.h"
#include "mem_internals.h"

#include <assert.h>
#include <sys/mman.h>

#define HEAP_SIZE 4096
#define HEAP_START ((void*)0x04040000)

void debug(const char* fmt, ... );

#define get_header(mem) \
    ((struct block_header*) (((uint8_t*) (mem)) - offsetof(struct block_header, contents)))

extern void* map_pages(void const*, size_t, int);

void test_success_memory_allocation(){
    debug("Test 1: success_memory_allocation\n");

    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    debug("\nInitializaing %d bytes\n", HEAP_SIZE);

    debug("Memory allocation: %d and %d bytes\n", 2048, 8192);
    void* allocated = _malloc(2048);
    assert(allocated);
    allocated = _malloc(8192);
    assert(allocated);

    heap_term();
    debug("\nTest 1 success!\n");
}

void test_realeasing_one_block() {
    debug("\nTest 2: realeasing_one_block\n");

    void* heap = heap_init(0);
    assert(heap);
    debug("\nInitializaing %d bytes\n", 0);

    debug("3 Blocks initialization...\n", 0);
    void* first_block = _malloc(100);
    void* second_block = _malloc(200);
    void* third_block = _malloc(300);

    assert(first_block);
    assert(second_block);
    assert(third_block);

    debug("free_first_block\n");
    _free(first_block);
    assert(get_header(first_block)->is_free);
    assert(!get_header(second_block)->is_free);
    assert(!get_header(third_block)->is_free);
    heap_term();
    debug("\nTest 2 success!\n");
}

void test_realeasing_two_blocks() {
    debug("\nTest 3: realeasing_two_blocks\n");

    void* heap = heap_init(0);
    assert(heap);
    debug("\nInitializaing %d bytes\n", 0);

    debug("3 Blocks initialization\n", 0);
    void* first_block = _malloc(100);
    void* second_block = _malloc(200);
    void* third_block = _malloc(300);

    assert(first_block);
    assert(second_block);
    assert(third_block);

    debug("free_two_blocks\n");
    _free(first_block);
    _free(second_block);
    assert(get_header(first_block)->is_free);
    assert(get_header(second_block)->is_free);
    assert(!get_header(third_block)->is_free);

    heap_term();
    debug("\nTest 3 success!\n");
}

void test_expand_region() {
    debug("\nTest 4: expand_region\n");

    struct region* heap = heap_init(0);
    assert(heap);
    debug("\nInitializaing %d bytes\n", 0);

    size_t initial_region_size = heap->size;

    _malloc(5 * HEAP_SIZE);
    size_t expanded_region_size = heap->size;
    debug("block initialization success\n", 0);

    assert(initial_region_size < expanded_region_size);

    heap_term();
    debug("\nTest 4 success!\n");
}

void test_selection_region() {
    debug("\nTest 5: selection_region\n");

    debug("\nFilling memory\n", 0);
    void* region = map_pages(HEAP_START, 100, MAP_FIXED);
    assert(region);
    debug("Trying to allocate in selected region\n", 0);
    void* selected_region = _malloc(100);
    assert(selected_region);
    assert(region != selected_region);

    heap_term();
    debug("\nTest 5 success!\n");
}

int main() {
    test_success_memory_allocation();
    test_realeasing_one_block();
    test_realeasing_two_blocks();
    test_expand_region();
    test_selection_region();

    debug("\nAll test passed!\n");
}
